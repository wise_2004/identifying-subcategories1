WITH SubcategorySales AS (
    -- Calculate sales for each subcategory from 1998 to 2001
    SELECT
        prod_subcategory,
        EXTRACT(YEAR FROM time_id) AS sales_year,
        SUM(amount_sold) AS total_sales
    FROM
        sh.sales
        JOIN sh.products ON sh.sales.prod_id = sh.products.prod_id
    WHERE
        EXTRACT(YEAR FROM time_id) BETWEEN 1998 AND 2001
    GROUP BY
        prod_subcategory, sales_year
),
PreviousYearSales AS (
    -- Calculate sales for the previous year for each subcategory
    SELECT
        prod_subcategory,
        sales_year + 1 AS previous_year,
        total_sales AS previous_year_sales
    FROM
        SubcategorySales
)
-- Identify subcategories with consistently higher sales from 1998 to 2001 compared to the previous year
SELECT DISTINCT
    s1.prod_subcategory
FROM
    SubcategorySales s1
    JOIN PreviousYearSales s2 ON s1.prod_subcategory = s2.prod_subcategory
                             AND s1.sales_year = s2.previous_year
WHERE
    s1.total_sales > s2.previous_year_sales;
